package com.ske.snakebaddesign.models;

public class Player {

    private String name;
    private Die die;
    private Square currentSquare;

    public Player(String name){
        this.name = name;
    }

    public void setDie(Die dies) {
        this.die = dies;
    }

    public void rollDie(){
        die.roll();
    }

    public void setCurrentSquare(Square currentSquare) {
        this.currentSquare = currentSquare;
    }

    public Square getCurrentSquare() {
        return currentSquare;
    }

    public String toString() {
        return name;
    }
}
