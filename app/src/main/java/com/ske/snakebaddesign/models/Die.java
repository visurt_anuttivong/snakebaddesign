package com.ske.snakebaddesign.models;

import java.util.Random;

public class Die {

    private int faceValue;
    private Random random;

    public Die(){
        random = new Random();
        roll();
    }

    public void roll(){
        faceValue = 1 + random.nextInt(6);
    }

    public int getFaceValue(){
        return faceValue;
    }

}