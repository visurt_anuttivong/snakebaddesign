package com.ske.snakebaddesign.models;

public class Square {

    private int position;

    public Square(int position){
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public boolean equals(Object o){
        if(this == o) return true;
        if(!(o instanceof Square)) return false;
        Square square = (Square) o;
        return position == square.position;
    }

    public int hashCode(){
        return position;
    }
}
