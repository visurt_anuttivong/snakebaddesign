package com.ske.snakebaddesign.models;

public class Game {

    private int boardSize = 6;

    private Board board;
    private Die die;

    private Player p1;
    private Player p2;
    private int turn;

    public Game(){
        reset();
    }

    public void reset(){
        board = new Board(boardSize);
        die = new Die();
        p1 = new Player("Player 1");
        p1.setDie(die);
        p2 = new Player("Player 2");
        p2.setDie(die);
        turn = 0;
    }

    public void start(){
        p1.setCurrentSquare(board.getStartSquare());
        p2.setCurrentSquare(board.getStartSquare());
    }

    public void currentPlayerTakeAction(){
        currentPlayer().rollDie();
    }

    public int getDieValue(){
        return die.getFaceValue();
    }

    public void movePlayer(){
        board.move(currentPlayer(),die.getFaceValue());
    }

    public void nextTurn(){
        turn++;
    }

    public int getBoardSize(){
        return boardSize;
    }

    public int getPlayer1Position(){
        return p1.getCurrentSquare().getPosition();
    }

    public int getPlayer2Position(){
        return p2.getCurrentSquare().getPosition();
    }

    public Player getWinner(){
        if(p1.getCurrentSquare().equals(board.getEndSquare())){
            return p1;
        } else if(p2.getCurrentSquare().equals(board.getEndSquare())){
            return p2;
        }
        return null;
    }

    public Player currentPlayer(){
        if(turn%2 == 0) return p1;
        return p2;
    }

}
