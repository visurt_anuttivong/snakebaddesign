package com.ske.snakebaddesign.models;

public class Board {

    private Square[] squares;

    public Board(int size){
        initSquares(size);
    }

    private void initSquares(int size){
        squares = new Square[size * size];
        for(int i=0;i<size * size;i++){
            squares[i] = new Square(i);
        }
    }

    public Square getStartSquare(){
        return squares[0];
    }

    public Square getEndSquare(){
        return  squares[squares.length - 1];
    }

    public void move(Player player, int distance){
        int pos = player.getCurrentSquare().getPosition();
        int newPos = pos + distance;

        if(newPos >= squares.length){
            int diff = newPos - squares.length;
            newPos = squares.length - 2 - diff;
        }

        Square newSquare = squares[newPos];
        player.setCurrentSquare(newSquare);
    }
}
