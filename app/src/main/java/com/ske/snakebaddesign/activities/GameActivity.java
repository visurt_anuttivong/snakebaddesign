package com.ske.snakebaddesign.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ske.snakebaddesign.R;
import com.ske.snakebaddesign.guis.BoardView;

import com.ske.snakebaddesign.models.Game;

public class GameActivity extends AppCompatActivity {

    private Game game;

    private BoardView boardView;
    private Button buttonTakeTurn;
    private Button buttonRestart;
    private TextView textPlayerTurn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        initComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
        resetGame();
    }

    private void initComponents() {
        boardView = (BoardView) findViewById(R.id.board_view);
        buttonTakeTurn = (Button) findViewById(R.id.button_take_turn);
        buttonTakeTurn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeTurn();
            }
        });
        buttonRestart = (Button) findViewById(R.id.button_restart);
        buttonRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetGame();
            }
        });
        textPlayerTurn = (TextView) findViewById(R.id.text_player_turn);
    }

    private void resetGame() {
        game = new Game();
        game.start();
        boardView.setGame(game);
    }

    private void takeTurn() {
        game.currentPlayerTakeAction();
        final String title = "You rolled a die";
        String msg = "You got " + game.getDieValue();
        final OnClickListener listener = new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                game.movePlayer();
                boardView.postInvalidate();
                textPlayerTurn.setText(game.currentPlayer().toString());

                if(game.getWinner() == null){
                    game.nextTurn();
                } else {
                    String title = "Game Over";
                    String msg = "";
                    OnClickListener listener = new OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            resetGame();
                            dialog.dismiss();
                        }
                    };
                    if (game.getPlayer1Position() == game.getBoardSize() * game.getBoardSize() - 1) {
                        msg = "Player 1 won!";
                    } else if (game.getPlayer2Position() == game.getBoardSize() * game.getBoardSize() - 1) {
                        msg = "Player 2 won!";
                    } else {
                        return;
                    }
                    displayDialog(title, msg, listener);
                }
                dialog.dismiss();
            }
        };
        displayDialog(title, msg, listener);
    }

    private void displayDialog(String title, String message, DialogInterface.OnClickListener listener) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", listener);
        alertDialog.show();
    }

}
